using Test 

function teste()
    @test checkWill([0,1]) == false 
    @test checkWill([1,4,6,7]) == true 
    @test checkWill([0,1,2,3,4,5,6,7,8,9]) == true 
    @test checkWill([]) == false 
    @test checkWill([4,7,1,6,-5]) == true 

    @test checkTaki([0,1]) == false
    @test checkTaki([1,4,6,7]) == false
    @test checkTaki([1,1,7,7]) == false 
    @test checkTaki([1,1,1,4,6,7,7,7]) == true

    @test checkJackson(10,[0,1]) == false
    @test checkJackson(42,[1,4,6,7,1,1,7,7]) == false 
    @test checkJackson(1000,[1,1,7,7,1,0,0,0]) == false 
    @test checkJackson(1234,[6,1,7,4,7,7,1,1,1,2,3,4]) == true

    @test checkWillBase(2,[6,1,7,4]) == false
    @test checkWillBase(6,[6,7,8,9,6,7,8,9]) == false
    @test checkWillBase(10,[0,1,2,3,4,5,6,7,8,9]) == true
    @test checkWillBase(2,[1,1,0,0,0,0,0,1,1,1,1,0]) == true
    @test checkWillBase(8,[1,4,0,3,6,9,0]) == true

    println("Testes acabaram!")
end 


function checkWill(cards)
count6 = 0
count1 = 0
count7 = 0 
count4 = 0
    for i in 1:length(cards)
        if cards[i] == 6
            count6 += 1 
        elseif cards[i] == 1 
            count1 += 1 
        elseif cards[i] == 7 
            count7 += 1 
        elseif cards[i] == 4 
            count4 +=1 
        end 
    end 
    if count6 >= 1 && count1 >= 1 && count7 >= 1 && count4 >= 1
        return true 
    else 
        return false
    end 
end 

function checkTaki(cards)
    count6 = 0
    count1 = 0
    count7 = 0 
    count4 = 0
        for i in 1:length(cards)
            if cards[i] == 6
                count6 += 1 
            elseif cards[i] == 1 
                count1 += 1 
            elseif cards[i] == 7 
                count7 += 1 
            elseif cards[i] == 4 
                count4 +=1 
            end 
        end 
        if count6 >= 1 && count1 >= 3 && count7 >= 3 && count4 >= 1
            return true 
        else 
            return false
        end 
end

function checkJackson(x,cards)
    WillTaki = checkTaki(cards)

    while x != 0
    resto = x % 10
    atual = 0
        for i in 1:length(cards)
            if cards[i] == resto
                atual += 1
            end
        end
        if atual == 0
            return false
        else
            x = div(x,10)
        end
    end 
    if WillTaki == true
        return true
    else
        return false
    end
end

function mudabase(b,n)
    soma = 0
    pot = 1
        while n > 0 
            dig = n % b 
            soma = soma + dig * pot
            n = n ÷ b 
            pot = pot * 10
        end
    return soma
end

function checkWillBase(b,cards)
    number = mudabase(b,6174)

    while number != 0
        resto = number % 10
        atual = 0 
        for i in 1:length(cards)
            if cards[i] == resto
                atual += 1 
            end
        end
        if atual == 0
            return false
        else
            number = div(number,10)
        end
    end
    return true
end

teste()